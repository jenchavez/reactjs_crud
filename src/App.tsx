import "./App.css";
import{
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";

import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Users from "./pages/Users";
import Posts from "./pages/Posts";

const router = createBrowserRouter([
{
    path: "/",
    element: <Home/>,

},

{
    path: "/login",
    element: <Login />,

},

{
  path: "/users",
  element: <Users />,

},

{
    path: "/register", 
    element: <Register />,
},

{
 path: "*",
  element: <div> Found 404 Error</div> 
},
{
  path: "/post",
  element: <Posts/>,
},

]);

function App(){
    return(
        <RouterProvider router =  {router} />
    );
}

export default App;