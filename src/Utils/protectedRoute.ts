import {redirect} from "react-router-dom";

export const ProtectedRoute = () => {
    const userToken = JSON.parse (
        localStorage.getItem ("Token") || '{"username": "", "password": ""}'
    );
    if (userToken["username"] == "" && userToken["password"]=="") {
        return redirect("/login");
    } else {
        return null;
    }
};

export const isLogin = () => {
    const userToken = JSON.parse(
        localStorage.getItem("Token") || '{"username": "", "password": ""}'
    );
    if (userToken["username"] != "" && userToken ["password"] != ""){
        return redirect("/");
    }else {
        return null;
    }
}