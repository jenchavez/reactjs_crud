export interface IProp{}

export interface IState {
    userList: User[];
}

interface User{
    id: string;
    name: string;
    username: string;
    email: string;
}