import React, {useState, useEffect } from "react";
import axios from "axios";
import { IState } from "./type";

const Users: React.FC<IState> = () => {
    const [userList, setUserList] = useState<IState["userList"]>([]);

    useEffect (() => {
        handleRequestUsers();
    }, []);

    const handleRequestUsers = async () => {
        try{
            const response = await axios.get ("https://jsonplaceholder.typicode.com/users");
            setUserList(response.data);
        }catch (error){
            alert ('ERROR!');
        }
    };

    return (
        <div>
            <table border={1}>
                <tbody>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                    </tr>
                    {userList.map((items)=> {
                        return(
                            <tr key={items.id}>
                                <td>{items.id}</td>
                                <td>{items.name}</td>
                                <td>{items.username}</td>
                                <td>{items.email}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default Users;

