import React, {useState, useEffect} from "react";
import "./style.css";
import axios from "axios";
import { IProp,IState } from "./type";
import { useNavigate } from "react-router-dom";

//MUI style
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';


const Post: React.FC<IProp> = () => {
    const nav = useNavigate();
    const [postList, setPostList] = useState<IState["userPost"]>([]);
    const [title, setTitle] = useState ('');
    const [message, setMessage] = useState('');
    
    useEffect (() => {
        handleRequestUsers();
    }, []);
    const handleRequestUsers = async () =>{
        try {
            const response = await axios.get (
                "http://localhost:5173/api/post"
            );
            setPostList(response.data);
        } catch (error) {
            console.log(error)
        }
    };

    const handleLogout = () =>{
        localStorage.removeItem("Token")
        nav("/");
       }

    const handleAddButton = async () => {
        try{
            const response = await axios.post("http://localhost:5173/api/post", {title:title, message:message});
            if (response.status == 200) {
                handleRequestUsers();
                setTitle("");
                setMessage("");
                console.log({status});
            }
        }catch(error){
            console.log (error);
        }
    };

    const handleChangeInInput = (e: any) => {
        const {name, value} = e.target;
        switch(name){
            case 'title':
                return setTitle(value);
            case 'message':
                return setMessage(value)
            default:
                return;
        }
    }

    const handleDeletePost = async (id: number) => {
        try{
            await axios.delete (`http://localhost:5173/api/post/${id}`)
            setPostList(prevPostList => prevPostList.filter(item => item.id !== id))
        }catch (error){
            console.log(error);
        }
    }
    return (
        <>
        
        <div>
            <h2>POST </h2>
            <span><a href="/"> Home </a>|</span>
            <span><a href="#"> About </a>|</span>
            <span><a href="/Posts"> Post </a>|</span>
            <span><a href="/users"> User </a>|</span>
            <span><a href="/login" onClick={handleLogout}>Logout</a>|</span>

            <label>Name </label>
            <input name="title" type="text" onChange={handleChangeInInput} value={title} /><br/>
            <label>Message </label>
            <textarea name="message" onChange={handleChangeInInput} value={message}></textarea>
            <button onClick={handleAddButton}> Add </button>

            <form>
                <TextField id="outlined-basic" label="Title" variant="outlined"/><br/>
                <label>Message </label>
                <Box component="form"
                    sx={{
                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                    }}
                    noValidate
                    autoComplete="off">

                    <TextField
                    id="outlined-multiline-flexible"
                    label="Message"
                    multiline
                    maxRows={5}
                    />
                </Box>
                <Button variant="contained">POST</Button>
            </form>
            <TableContainer>
                <Table  sx={{ minWidth: 650 }} aria-label="simple table" >
                    <TableHead>
                        <TableRow>
                            <TableCell>Title</TableCell>
                            <TableCell>Message</TableCell>
                            <TableCell>Post</TableCell>
                            <TableCell>Status</TableCell>
                            <TableCell>Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>

                    {postList.map((items) => {
                        return (
                            <TableRow key={items.id}>
                            <TableCell>{items.title}</TableCell>
                            <TableCell>{items.message}</TableCell>
                            <Button variant="contained" color="success">Edit</Button>
                            <Button variant="contained" color="warning" onClick={()=>handleDeletePost}>Delete</Button>
                        </TableRow>

                        )
                    })}
                </TableBody>
                </Table>
            </TableContainer>
        </div>
        </>
       
    )
}

export default Post;