export interface IProp{}

export interface IState {
    userPost: userPost[];
}

interface userPost{
    id: number;
    title: string;
    message: string;
}