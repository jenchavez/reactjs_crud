

export interface IProp {}

export interface IState {
    temp_input: string;
    temp_message: string;
    todo: ITodo[];
    showDataTable: boolean;
    editData: ITodo
}

export interface ITodo {
    id: string;
    input: string;
    message: string;
}


export interface ITodoDisplayProp {
    inputlist: ITodo[];
    showDataTable: boolean;
    handlerDelete: (value: any) => void;
    handlerEdit: (value: any) => void;
    handleEditOnChange: (event: any) => void;
    handeUpdateButton: (event: any) => void;
    editData?: ITodo
}

export interface ITodoFormProp {
    inputHandler: (value: any) => void;
    messageHandler: (value: any) => void;
    btnAddHandler: () => void;
    handleCheckShowTable: (value: any) => void;
    handleButtonLogout: (value: any) => void;
    temp_input: string;
    temp_message: string;
}
