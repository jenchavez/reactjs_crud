import React, { useState } from "react";
import { ITodo, ITodoDisplayProp, ITodoFormProp, IState } from "./type";
import "./style.css";

import TodoDisplay from "./components/TodoDisplay";
import TodoForm from "./components/TodoForm";
import { useNavigate } from "react-router-dom";

const Home = () => {
  const [temp_input, setTempInput] = useState("");
  const [temp_message, setTempMessage] = useState("");
  const [todo, setTodo] = useState<ITodo[]>([]);
  const [showDataTable, setShowDataTable] = useState(false);
  const [editData, setEditData] = useState<ITodo>({ id: "", input: "", message: "" });

  const nav = useNavigate();

  const inputHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTempInput(event.target.value);
  };

  const messageHandler = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setTempMessage(event.target.value);
  };

  const btnAddHandler = () => {
    const newInputList = [
      ...todo,
      {
        id: `${new Date().getTime() / 1000}`,
        input: temp_input,
        message: temp_message,
      },
    ];
    setTodo(newInputList);
    setTempInput("");
    setTempMessage("");
  };

  const handleCheckShowTable = (event: React.ChangeEvent<HTMLInputElement>) => {
    setShowDataTable(event.target.checked);
  };

  const handleActionDelete = (id: string) => {
    const deleteConfirmation = window.confirm("Please confirm if you would like to proceed in data deletion");
    if (deleteConfirmation) {
      const newInputList = todo.filter((item) => item.id !== id);
      setTodo(newInputList);
    }
  };

  const handleEditButton = (item: ITodo) => {
    setEditData({ id: item.id, message: item.message, input: item.input });
  };

  const handleEditOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    if (name === "editInput") {
      setEditData({ ...editData, input: value });
    } else if (name === "editMessage") {
      setEditData({ ...editData, message: value });
    }
  };

  const handeUpdateButton = () => {
    const newList = todo.map(items => {
      if (items.id === editData.id) {
        return editData;
      }
      return items;
    });
    setTodo(newList);
    setEditData({ id: '', input: '', message: '' });
  };

  const handleButtonLogout = () => {
    nav('/login');
  }

  return (
    <>
      <div className="mainflex">

        <TodoForm
          btnAddHandler={btnAddHandler}
          inputHandler={inputHandler}
          messageHandler={messageHandler}
          temp_input={temp_input}
          temp_message={temp_message}
          handleCheckShowTable={handleCheckShowTable}
          handleButtonLogout={handleButtonLogout}
        />
      
      
      <TodoDisplay
        handlerEdit={handleEditButton}
        editData={editData}
        handlerDelete={handleActionDelete}
        showDataTable={showDataTable}
        inputlist={todo}
        handleEditOnChange={handleEditOnChange}
        handeUpdateButton={handeUpdateButton}
      />
      </div>
    
    </>
  );
}

export default Home;
