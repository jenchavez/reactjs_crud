import { ITodoFormProp } from "../type";

function TodoForm (props: ITodoFormProp){
    const {inputHandler, messageHandler, btnAddHandler, temp_input, temp_message, handleCheckShowTable, handleButtonLogout} = props;

    return(
        <div className="firstflex">
            <h1>Home Page <button className="btn btn-primary" onClick={handleButtonLogout}>Logout</button> </h1>
            <input name="title"
            type="text"
            onChange={inputHandler}
            value={temp_input}
            />
            <br />
            <textarea
            name="message"
            onChange={messageHandler}
            value={temp_message}>
            </textarea>
            <br />
            <button className="btn"
            onClick={btnAddHandler}>Add</button>

            <input type="checkbox" onChange={handleCheckShowTable} />{" "}
          <span>Show Table</span>
         

        </div>
    );

}

export default TodoForm;