import "../style.css";
import { ITodoDisplayProp } from "../type";

function TodoDisplay(props: ITodoDisplayProp) {
  const { inputlist, showDataTable,  editData, handlerDelete, handlerEdit, handleEditOnChange, handeUpdateButton } = props;

  return (
    <div className="secondflex">
      {!showDataTable ? (
        <></>
      ) : (
        <table border={1}>
          <tbody>
            <tr>
              <th>Title</th>
              <th>Message</th>
              <th>Action</th>
            </tr>
            {inputlist.map((items, index) => {
              if (items.id === editData?.id && items.id) {
                return (
                  <tr key={items.id + index}>
                    <td>
                      <input name="editInput" type="text" value={editData.input} onChange={handleEditOnChange}/>
                    </td>
                    <br/>
                    <td>
                      <input name="editMessage" type="text" value={editData.message} onChange={handleEditOnChange}/>
                    </td>
                    <td>
                      <button className="btn-update"onClick={handeUpdateButton}>Update</button>
                      <button className="btn-cancel"onClick={handeUpdateButton}>Cancel</button>
                    </td>
                  </tr>
                );
              } else {
                return (
                  <tr key={items.id + index}>
                    <td>
                      <span>{items.input}</span>
                    </td>
                    <td>{items.message}</td>
                    <td>
                      <button
                        className="edit-btn"
                        onClick={() => handlerEdit(items)}
                      >
                        Edit
                      </button>
                      <button
                        onClick={() => handlerDelete(items.id)}
                        className="delete-btn"
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              }
            })}
          </tbody>
        </table>
        
      )}
    </div>
  );
}

export default TodoDisplay;
