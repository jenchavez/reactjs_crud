import React from "react";
import "./style.css"
import { RegisterFormProp } from "./type";
import { useNavigate } from "react-router-dom";

const Register: React.FC<RegisterFormProp> = () => {
    const navigate = useNavigate();

    const btnRegister = () => {
        navigate("/login");
    };


    return (
        <div className="basic-register-container">
            <h2>Register Here</h2>
            <form>
                <input type="text" title="setUser" placeholder="username" />
                <input type="text" title="setPass" placeholder="password" />
                <input type="text" title="setPass1" placeholder="confirm password" />
                <button  className="btn-register" onClick={btnRegister}>Register</button> <br />
                <a className="signIn" href="http://localhost:5173" >Login here if you already have an account? </a>
            </form>
        </div>
    )
}

export default Register;