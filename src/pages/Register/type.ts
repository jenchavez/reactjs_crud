export interface IProp{};

export interface RegisterFormProp {
    setUserName: (event: any) => void;
    setPassWord: (event: any) => void;
    btnRegister: () => void;
    username: string;
    password: string;
}