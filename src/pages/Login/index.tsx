import React from "react";
import { IProp } from "../Login/type";
import {useNavigate} from 'react-router-dom'
import "./style.css";
 
 
 
const Login: React.FC<IProp> = () => {

    const nav = useNavigate();
    const handleLogin = () => {
        nav("/")
    }
 
    return (
    <div className="login-container">
        <h2>Login</h2>

        <form>
            <input type="text" title="user" placeholder="username"/>
            <input type="password" title="pword" placeholder="password"/>
            <button className="btn-login" onClick={handleLogin}>Login</button>
            <a className="forgot" href="http://localhost:5173/register">Register</a>
                
        </form>
    </div>
    )
}
 
export default Login;